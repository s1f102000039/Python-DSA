# https://leetcode.com/problems/balanced-binary-tree/
# Depth first search

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def isBalanced(root) -> bool:
    if not root:
        return True
    
    def dfs(root):
        if not root:
            return [True, 0]
        
        left = dfs(root.left)
        right = dfs(root.right)
        balanced = left[0] and right[0] and abs(left[1] - right[1]) <= 1
        
        if not balanced:
            return False
        
        return [balanced, max(left[1], right[1]) + 1]
    
    return dfs(root)

# Optimized
def isBalanced(root):
    def check(root):
        if not root:
            return 0
        
        left = check(root.left)
        if left == -1:
            return -1
        right = check(root.right)
        if right == -1:
            return -1
        
        if abs(left - right) > 1:
            return -1
        return max(left, right) + 1
    
    return check(root) != -1
    