# https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
# Kadane's algorithm 
# 2 pointers: If the graph keeps going up -> Keep calculating the difference of that range
# Once the graph starts going down -> The difference won't get any bigger,
# If there is a bigger value above then the smaller value will always produce a bigger difference
# Scan all value from l->r, 
# We are basically finding the maximum difference of different ranges

class Solution:
    def maxProfit(self, prices: list[int]) -> int:
        left, right = 0, 1
        maxPrice = 0
        
        while right < len(prices):
            if prices[left] > prices[right]:
                left = right
                right += 1
            else: 
                maxPrice = max(maxPrice, prices[right] - prices[left])
                right += 1
        
        return maxPrice


# O(n^2) solution
# class Solution:
#     def maxProfit(self, prices: List[int]) -> int:
#         curMax = 0
        
#         for i in range(len(prices)):
#             for j in range(i, len(prices)):
#                 current = prices[i]
#                 if prices[j] < prices[i]:
#                     continue
#                 else: curMax = max(curMax, prices[j] - prices[i])
#         return curMax 