# https://leetcode.com/problems/binary-search/

def search(nums, target):
    lo = 0
    hi = len(nums) - 1
    def rec(nums, target, lo, hi):
        if hi >= lo:
            index = (lo + hi) // 2
            print(nums,nums[index], lo, hi, index)
            if target < nums[index]:
                return rec(nums, target, lo, index - 1)
            elif target > nums[index]: 
                return rec(nums, target, index + 1, hi)
            elif target == nums[index]:
                return index
        else:
            return -1
        
    return rec(nums, target, lo, hi)

print(search([-1,0,3,5,9,12],  2))


# [-1, 0, 3, 5, 9, 12] 3 0 5 2
# [-1, 0, 3, 5, 9, 12] -1 0 1 0
# [-1, 0, 3, 5, 9, 12] 0 1 1 1
# -1

# [-1, 0, 3, 5, 9, 12] 3 0 5 2
# [-1, 0, 3, 5, 9, 12] 0 0 2 1
# [-1, 0, 3, 5, 9, 12] 3 2 2 2
# [-1, 0, 3, 5, 9, 12] 3 2 2 2