# https://leetcode.com/problems/diameter-of-binary-tree/
"""
    A DP problem, basically just keep track of the diameter of every single subtree
    And keep a maxDia variable to keep track of which subtree has the largest diameter
    'nonlocal' tells the interpreter to check the closest enclosing scope for the variable 
    (excluding global scope) instead of the current local scope
"""

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def diameterOfBinaryTree(root) -> int:
    maxDia = 0

    def dfs(root):
        nonlocal maxDia
        if not root:
            return 0
        left = dfs(root.left)
        right = dfs(root.right)

        diameter = left + right
        maxDia = max(maxDia, diameter)

        return max(left, right) + 1
    dfs(root)

    return maxDia
