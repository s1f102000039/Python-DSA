# https://leetcode.com/problems/flood-fill/
# DFS problem
# Start with the starting pixel, if it's already the newColor or out of bound then return. 
# If not then turn it to new color, continue DFS for all directional neighbor 

def floodFill(image, sr, sc, newColor):
    rows, cols = len(image) - 1, len(image[0]) - 1
    color = image[sr][sc]
    
    def dfs(row, col, color):
        if row < 0 or row > rows or col < 0 or col > cols:
            return
        if image[row][col] != color or image[row][col] == newColor:
            return
        image[row][col] = newColor
        dfs(row + 1, col, color)
        dfs(row - 1, col, color)
        dfs(row, col + 1, color)
        dfs(row, col - 1, color)
    dfs(sr, sc, color)
    return image

print(floodFill([[1,1,1],[1,1,0],[1,0,1]], sr = 1, sc = 1, newColor = 0))