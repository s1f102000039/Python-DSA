# https://leetcode.com/problems/maximum-subarray/

def maxSubArray(nums: list[int]) -> int:
    if not nums:
        return -1
    curr = 0
    res = nums[0]
    for num in nums:
        curr += num
        res = max(res,curr)
        if curr < 0:
            curr = 0
    return res
print(maxSubArray([-2, -1]))