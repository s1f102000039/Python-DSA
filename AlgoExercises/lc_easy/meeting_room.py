def find(time):
    sortedTime = sorted(time, key = lambda x: x[0])
    currRight = None
    for left, right in sortedTime:
        if not currRight:
            currRight = right
        elif left < currRight and right != currRight:
            return False
        currRight = right
    return True
        
print(find([[7,10],[2,4]]))