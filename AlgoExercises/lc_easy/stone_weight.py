# from collections import Counter
# def getMinimumDifference(a, b):
#     ans = []
#     for i in range(len(a)):
#         str1 = a[i]
#         str2 = b[i]
#         if len(str1) != len(str2):
#             ans.append(-1)
#         else:
#             strCnt = Counter(str1)
#             str2Cnt = Counter(str2)
#             print((strCnt - str2Cnt))
#             ans.append(sum((strCnt - str2Cnt).values()))
#     return ans
# print(getMinimumDifference(['a', 'jk', 'abb', 'mn', 'abc'], ['bb', 'kj', 'bbc', 'op', 'def']))

def theFinalProblem(target):
    target_array = [int(i) for i in target]
    min_flips = 0
    for bit in target_array:
        if min_flips % 2 == 0:
            if bit == 1:
                min_flips += 1
        else:
            if bit == 0:
                min_flips += 1
    return min_flips
            
print(theFinalProblem('01011'))