# https://leetcode.com/problems/two-sum/

class Solution:
    def twoSum(self, nums: list[int], target: int) -> list[int]:
        hashmap = {}
        for i in range(len(nums)):
            if target - nums[i] in hashmap:
                return [i, hashmap[i]]
            hashmap[nums[i]] = i
        return "No solution"


solution = Solution()
print(solution.twoSum([3,2,4], 6))
