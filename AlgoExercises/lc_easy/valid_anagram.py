# https://leetcode.com/problems/valid-anagram/
# Check to see if key in dictionary: if key in dict.keys()

def isAnagram(s: str, t: str) -> bool:
    dict1 = {}
    dict2 = {}
    
    for i in s:
        if i not in dict1.keys():
            dict1[i] = 1
        else: dict1[i] += 1
    
    for i in t:
        if i not in dict2.keys():
            dict2[i] = 1
        else: dict2[i] += 1
    
    return dict1 == dict2

dict = {}
dict['a'] = 1
print(isAnagram("anagram", "nagaram"))