# https://leetcode.com/problems/valid-palindrome/

def isPalindrome(s: str) -> bool:
    sanitized = [i.lower() for i in s if i.isalnum()]
    return sanitized == sanitized[::-1]

print(isPalindrome('A man, a plan, a canal: Panama'))