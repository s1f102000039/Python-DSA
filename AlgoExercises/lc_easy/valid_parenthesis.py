# https://leetcode.com/problems/valid-parentheses/

class Solution:
    def isValid(self, s: str) -> bool:
        stack = []
        balanced = True
        for i in s:
            if i in ['(', '{', '[']:
                stack.append(i)
            elif i in [')', '}', ']'] and stack:
                cur_paren = stack.pop()
                if cur_paren == '(' and i != ')':
                    return False
                if cur_paren == '{' and i != '}':
                    return False
                if cur_paren == '[' and i != ']':
                    return False
            else:
                balanced = False
        if stack:
            balanced = False
        return balanced
        
                