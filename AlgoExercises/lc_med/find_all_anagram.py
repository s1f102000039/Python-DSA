import string

"""
    Sliding windows technique, instead of recreating the window with every slide
    Remove the last elements and use the new elements
"""

def findAnagrams(s: str, p: str) -> list[int]:
    ALPHABET = string.ascii_lowercase
    p_signature = [0 for _ in ALPHABET]
    first_window = [0 for _ in ALPHABET]
    ans = []

    for char in s[0:len(p)]:
        first_window[ord(char) - ord(ALPHABET[0])] += 1
    
    for char in p:
        p_signature[ord(char) - ord(ALPHABET[0])] += 1
        
    if first_window == p_signature:
        ans.append(0)

    left = 0
    for right in range(len(p), len(s)):
        first_window[ord(s[left]) - ord(ALPHABET[0])]  -= 1
        first_window[ord(s[right]) - ord(ALPHABET[0])]  += 1
        left += 1
        if first_window == p_signature:
            ans.append(left)

    return ans

print(findAnagrams("cbaebabacd","abc"))