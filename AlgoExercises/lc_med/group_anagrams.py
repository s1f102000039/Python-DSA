# https://leetcode.com/problems/group-anagrams/
from collections import defaultdict

class Solution:
    def groupAnagrams(self, strs: list[str]) -> list[list[str]]:
        res = []
        hashmap = defaultdict(list)
        for s in strs:
            hashmap[tuple(sorted(s))].append(s)
        for value in hashmap.values():
            res.append(value)
        return res