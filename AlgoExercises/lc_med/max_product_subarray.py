# https://leetcode.com/problems/maximum-product-subarray/submissions/
"""
    The problem boils down to whether we have an even or odd number of negative elements
    If we have an even number of -ve(0 counts as odd), then the maximum subarray will be the whole array
    If we have an odd number of -ve, then the maximum subarray will either end at the first or last negative elements
    But we have to consider both the left subarray and the right subarray ending at those negative elements
    -> To do this we simply need to iterate through the array, if from the left -> delimited at last -ve, from the right -> delimited at first -ve
"""

def maxProduct(nums: list[int]) -> int:
    if not nums:
        return 0
    res, currMax = nums[0], 1
    
    for num in nums:
        currMax *= num
        res = max(res, currMax)
        
        if currMax == 0:
            currMax = 1

    currMax = 1
    
    for num in nums[::-1]:
        currMax*= num
        res = max(res, currMax)
        if currMax == 0:
            currMax = 1
        
    return res
        
        
    
    