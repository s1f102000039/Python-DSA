# https://leetcode.com/problems/remove-nth-node-from-end-of-list/
# https://www.youtube.com/watch?v=XVuQxVej6y8
# In order to offset the none by 1 create a dummy node with dummy.next = head
# Use 2 pointers left, right with left delayed from right by n. When right reaches the end left is exactly where we need it to be.

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        dummy = ListNode()
        dummy.next = head
        left = dummy 
        right = head
        
        for _ in range(n):
            right = right.next
        
        while right != None:
            right = right.next
            left = left.next
        
        left.next = left.next.next
        
        return dummy.next







# Naive 2-pass solution
# class Solution:
#     def removeNthFromEnd(self, head: Optional[ListNode], n: int) -> Optional[ListNode]:
#         length = 0
#         dummy = head
        
#         if head.next == None:
#             return head.next
        
#         while dummy != None:
#             length += 1
#             dummy = dummy.next
#         dummy = head
        
#         if n == length:
#             return head.next
        
#         for i in range(length - n - 1):
#             dummy = dummy.next
        
#         dummy.next = dummy.next.next
        
#         return head