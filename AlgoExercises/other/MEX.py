def Mex(lst, x):
    if not lst:
        return -1
        
    mex = 0
    appeared = set()

    for num in lst:
        remainder = num%x
        while remainder in appeared:
            remainder = remainder + x
        appeared.add(remainder)
    while mex in appeared:
        mex = mex + 1
        
    return mex

print(Mex([0,1,2,1,3],3))
    
         