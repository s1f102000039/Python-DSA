"""
    What I tried: Find out how much each person has bribed -> Is very hard to be calculated or straight up impossible
    It's easier to find how much a person has BEEN bribed instead
    So for each elements, search the entire subarray before it, 
    any elements that is larger than it means it has bribed it at one point in time

    **NOTE: A person can bribe the person in front of them, but the person in front of them can't bribe the person behind them
    **NOTE 2: Even if an element is still in its' original position doesn't mean it hasn't been bribed, 

    it could have bribed back its' position
    We can optimize the above algorithm, instead of searching the entire subarray before it,
    We can search only from its' original position + 1 until its current index
    Why? Because if an element has bribed it once, they can only go as far as the original + 1 since they only have 1 bribe left
"""

def minimumBribes(q):
    min_bribes = 0
    chaotic = False

    for index, badge in enumerate(q):
        if badge - index - 1 > 2:
            print("Too chaotic")
            chaotic = True
            break
        else:
            for j in range(max(badge - 2, 0), index):
                if q[j] > badge:
                    min_bribes += 1
    if not chaotic:
        print(min_bribes)



if __name__ == '__main__':
    t = int(input().strip())

    for t_itr in range(t):
        n = int(input().strip())

        q = list(map(int, input().rstrip().split()))

        minimumBribes(q)
