import random
import time
import string
import re


def gen_test(big_s_l=100000, pattern_l=10, fail_ratio=0.2):
  big_s = ''.join(random.choices(string.ascii_lowercase + ' ', k=big_s_l))
  # print(big_s)

  is_failed = random.random() < fail_ratio
  # print(random.random(), is_failed)

  ast_start, left_count, right_count = None, None, None
  while ast_start is None or left_count is None or big_s[ast_start + left_count] == ' ':
    ast_start = random.randint(0, big_s_l - pattern_l - 1)
    ast_ratio = random.random()
    left_count = int(ast_ratio * (pattern_l-1))
    right_count = (pattern_l-1) - left_count

  if is_failed:
    pattern_left = ''.join(random.choices(string.ascii_lowercase, k=left_count))
    pattern_right = ''.join(random.choices(string.ascii_lowercase, k=right_count))
  else:
    pattern_left = big_s[ast_start:ast_start+left_count]
    pattern_right = big_s[ast_start + left_count+1:ast_start + left_count +1+ right_count]

  pattern = pattern_left + '*' + pattern_right
  return big_s, pattern


def solve(big_s: str, pattern: str):
  pattern_left, pattern_right = pattern.split('*')
  if len(pattern_left) > len(pattern_right):
    pattern_big, pattern_small, pattern_dir = pattern_left, pattern_right, 'left'
  else:
    pattern_big, pattern_small, pattern_dir = pattern_right, pattern_left, 'right'

  ind = 0
  while True:
    o = big_s.find(pattern_big, ind)
    if o == -1:
      break
    ind = o+1
    try:
      if pattern_dir == 'left':
        start_ = o + len(pattern_big) + 1
        ast_ = o + len(pattern_big)
      else:
        start_ = o - len(pattern_small) - 1
        ast_ = o - 1
      end_ = start_ + len(pattern_small)

      if big_s[start_:end_] == pattern_small:
        if big_s[ast_].isalpha():
          return 'yes'
    except IndexError:
      continue

  return 'no'


def solve2(big_s: str, pattern: str):
  for i in string.ascii_lowercase:
    subs = pattern.replace('*', i)
    if subs in big_s:
      return 'yes'
  return 'no'


def solve3(big_s: str, pattern: str):
  if re.search(pattern.replace('*', '[a-zA-Z]'), big_s):
      return 'yes'
  return 'no'


tests = [gen_test() for _ in range(500)]

start = time.perf_counter()

# print([solve(t[0], t[1]) for t in tests])
for t in tests:
  solve(t[0], t[1])

end = time.perf_counter()
print('OPTIMIZED TOOK: ', end - start)
start2 = time.perf_counter()

for t in tests:
  solve2(t[0], t[1])

end2 = time.perf_counter()
print('26-LETTERS TOOK: ', end2 - start2)
start3 = time.perf_counter()

for t in tests:
  solve3(t[0], t[1])

end3 = time.perf_counter()
print('REGEX REPLACE TOOK: ', end3 - start3)