from collections import defaultdict

"""
    You only need to check frequency, don't get fooled by the question and use an extra array
    Remember for command '2', check that the frequency is >0 before removing AKA -= 1, see line 17
"""

# Complete the freqQuery function below.
def freqQuery(queries):
    hashmap = defaultdict(int)
    freq2 = defaultdict(int)
    result = []
    for query in queries:
        command = query[0]
        value = query[1]
        
        if command == 1:
            if freq2[hashmap[value]]:
                print("Got here", hashmap[value])
                freq2[hashmap[value]] -= 1
            hashmap[value] = hashmap.get(value, 0) + 1
            freq2[hashmap[value]] += 1
        
        elif command == 2:
            if value in hashmap and hashmap[value] > 0:
                freq2[hashmap[value]] -= 1
                hashmap[value] -= 1
                freq2[hashmap[value]] += 1

        elif command == 3:
            if freq2[value] > 0:
                result.append(1)
            else:
                result.append(0)
    print(freq2)
    return result  

if __name__ == '__main__':
    q = int(input().strip())

    queries = []

    for _ in range(q):
        queries.append(list(map(int, input().rstrip().split())))

    ans = freqQuery(queries)
