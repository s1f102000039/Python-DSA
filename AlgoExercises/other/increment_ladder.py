def increase(nums, target):
    filled = set()
    index = 0
    sorted_nums = [i for i in sorted(nums) if i > -target]
    for i in sorted_nums:
        if i in filled:
            filled.add(i + target)
            index += 1
            continue
        
        if i < index:  
            new = i + target
            if new > index:
                filled.add(new)
            elif new == index:
                index += 1
        
        elif i > index:
            new = i - target
            if new < index:
                continue
            elif new == index:
                index += 1
            elif new > index and new not in filled:
                filled.add(i)
        elif i == index:
            index += 1
    return index
            


print(increase([3,4,5], 1))

