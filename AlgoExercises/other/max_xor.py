def maximizingXor(l, r, k):
    # Write your code here
    max_xor = 1
    for i in range(l, r + 1):
        for j in range(l, r + 1):
            res = i^j
            if res <= k:
                max_xor = max(max_xor, res)
    return max_xor
print(maximizingXor(3,5,6))