# bit.ly/qwsSdwSd

def minimumSwaps(arr):
    swap = 0
    index = 0
    while index < len(arr):
        num = arr[index]
        if arr[num - 1] != num:
            arr[index], arr[num - 1] = arr[num - 1], arr[index]
            swap += 1
        else:
            index += 1
    return swap

print(minimumSwaps([4,2,1,3]))

# Non-working code to learn about reevaluation order

def minimumSwaps(arr):
    swap = 0
    index = 0
    while index < len(arr):
        if arr[arr[index] - 1] != arr[index]:
            arr[index], arr[arr[index]  - 1] = arr[arr[index]  - 1], arr[index]
            swap += 1
        else:
            index += 1
    return swap

print(minimumSwaps([4,3,1,2]))
