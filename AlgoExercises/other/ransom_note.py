"""
    Check length of magazine < length of note not '!='
    Also have to keep count of each word in the magazine since each word can only be used once
"""

def checkMagazine(magazine, note):
    if len(magazine) < len(note):
        print("No")
        return
    wordDict = {}
    for word in magazine:
        if word not in wordDict:
            wordDict[word] = 1
        else:
            wordDict[word] += 1
    for word in note:
        if word not in wordDict or wordDict[word] <= 0:
            print("No")
            return
        else:
            wordDict[word] -= 1
    print("Yes")

if __name__ == '__main__':
    first_multiple_input = input().rstrip().split()

    magazine = input().rstrip().split()

    note = input().rstrip().split()

    checkMagazine(magazine, note)