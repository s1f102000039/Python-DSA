
"""
    If the array length is n then after n shift 
    the array will be back to the start
    Therefore if d is larger than n we can simply do d = d%n 
    By shifting left n times we are basically 
    moving the starting point of the list n elements to the right
    1 2 3 4
    ^
    After left shifting twice
    1 2 3 4
        ^ 
    And we have 3 4 1 2   
""" 

def rotLeft(a, d):
    arr_len = len(a)
    temp = []
    d = d % arr_len
    for _ in range(arr_len):
        if d >= arr_len:
            d = 0
        print(d)
        temp.append(a[d])
        d+=1
    return temp

print(rotLeft([1,2,3,4,5], 12))