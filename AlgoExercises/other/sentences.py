from collections import defaultdict

def tupleSortedKey(key):
    return tuple(sorted(key))

def sentence_possibilities(anagram_map, sentence):
    words = sentence.split()
    sentence_count = 1
    for word in words:
        key = tupleSortedKey(word)
        anagram_count = len(anagram_map[key])
        if anagram_count > 0:
            sentence_count *= anagram_count
    return sentence_count


def sentences(wordset, sentences):
    result = []
    anagram_map = defaultdict(list)

    for word in wordset:
        key = tupleSortedKey(word)
        anagram_map[key].append(word)

    for sentence in sentences:
        result.append(sentence_possibilities(anagram_map, sentence))
    return result

print(sentences(["bats","tabs","in","cat","act"],["cat the bats", "in the act", "act tabs in"]))

# from collections import defaultdict

# def tupleSortedKey(key):
#     return tuple(sorted(key))

# def sentences(wordset, sentences):
#     result = []
#     hashmap = defaultdict(list)

#     for word in wordset:
#         key = tupleSortedKey(word)
#         hashmap[key].append(word)

#     for sentence in sentences:
#         words = sentence.split()
#         sentenceCnt = 1
#         for word in words:
#             key = tupleSortedKey(word)
#             anagramList = len(hashmap[key])
#             if anagramList > 0:
#                 sentenceCnt *= anagramList
#         result.append(sentenceCnt)
#     return result

# print(sentences(["bats","tabs","in","cat","act"],["cat the bats", "in the act", "act tabs in"]))