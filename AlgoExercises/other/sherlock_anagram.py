"""
    For every substring it can be represented by a list of all the characters that appears 
    'm': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    So take that list and turns it into a key
    **NOTE: Lists are not hashable in Python(can't be turned into a key)
    So we must first convert `signature` into a tuple which is hashable
    And then the value is the times that that signature appears
    Let's say it appears n times, that means we can make n(n-1)/2 combination of anagram
    Example:
    'm' appears 3 times at index 0,1,2
    We can make anagram pairs [0,1] [0,2] [1,2] which is just 3! = n(n-1)/2 
"""

import string


def sherlockAndAnagrams(s):
    ALPHABET = string.ascii_lowercase

    def find_all_substring():
        anagramCount = 0
        hashmap = {}
        signature = [0 for _ in ALPHABET]
        for i in range(len(s)):
            for j in range(i, len(s)):
                signature = [0 for _ in ALPHABET]
                subString = s[i:j+1]
                for char in subString:
                    signature[ord(char) - ord(ALPHABET[0])] += 1
                signature = tuple(signature)
                hashmap[signature] = hashmap.get(signature, 0) + 1
        for i in hashmap.values():
            anagramCount += i*(i-1)/2
        return anagramCount
    return find_all_substring()


print(sherlockAndAnagrams('mom'))
