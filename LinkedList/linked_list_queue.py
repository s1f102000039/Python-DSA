class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class Queue:
    def __init__(self):
        self.head = self.rear = None

    def is_empty(self):
        return self.head == None

    def enqueue(self, data):
        temp = Node(data)

        if self.rear == None:
            self.head = self.rear = temp

        else:
            self.rear.next = temp
            self.rear = temp

    def dequeue(self):
        if self.is_empty():
            return("Empty stack")

        temp = self.head

        if(self.head == None):
            self.rear = None

        else:
            self.head = self.head.next
        return temp

    def __str__(self):
        current = self.head
        res = []
        while current != None:
            res.append(current.data)
            current = current.next
        return str(res)


if __name__ == '__main__':
    q = Queue()
    q.enqueue(10)
    q.enqueue(20)
    q.enqueue(20)
    q.enqueue(20)
    q.dequeue()

    print(q)
