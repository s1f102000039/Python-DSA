class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
    
    def set_data(self, new_data):
        self.data = new_data
    
    def set_next(self, new_next):
        self.next = new_next
    
    def get_data(self):
        return self.data

    def get_next(self):
        return self.next


class UnorderedList:
    def __init__(self):
        self.head = None
        self.tail = None
    
    def isEmpty(self):
        return self.head == None
    
    def get_tail(self):
        return self.tail.get_data()

    def add(self,item):
        new_item = Node(item)
        
        if self.isEmpty():
            self.tail = new_item

        new_item.next = self.head
        self.head = new_item

    def size(self):
        current = self.head
        nodes = 0
        while current != None:
            nodes += 1
            current = current.get_next()
        return nodes
    
    def search(self, item):
        current = self.head
        found = False

        while current != None and not found:
            if current.get_data() == item:
                found = True
            current = current.next
        
        return found
    
    def remove(self, item):
        current = self.head
        previous = None
        found = False

        while current != None and not found:
            if current.get_data() == item:
                found = True
            else:
                previous = current
                current = current.get_next()
        
        if previous == None: #The item to be removed is the first item
            self.head = current.get_next()
        elif current == self.tail: #Move the tail when removing last item in list
            self.tail = previous
            previous.set_next(None)
        else:
            previous.set_next(current.get_next())

    def insert(self, index, item):
        current_ind = 0
        current = self.head
        previous = None

        while current != None and current_ind != index:
            previous = current
            current = current.get_next()
            current_ind += 1
        
        if previous == None: # If inserting into the first of the list, might or might not have following elements
            new_item = Node(item)
            new_item.set_next(self.head)
            self.head = new_item
            if previous == self.tail: # If the start of the list is the same as the end of the list
                self.tail = new_item
        
        elif previous == self.tail: #Inserting into the end of the list
            self.append(item)

        else:
            new_node = Node(item)
            previous.set_next(new_node)
            new_node.set_next(current)
        

    def append(self, item):
        new_node = Node(item)
        if self.isEmpty():
            self.head = new_node
            self.tail = new_node
        
        else:
            self.tail.set_next(new_node)
            self.tail = new_node

            
    def __str__(self):
        list = []
        current = self.head
        while current != None:
            list.append(current.get_data())
            current = current.get_next()
        return str(list)

        
def main():       
    mylist = UnorderedList()

    mylist.insert(0, 1)
    mylist.insert(0, 2)
    mylist.insert(0, 3)
    mylist.insert(0, 4)
    mylist.insert(5, 5)
    mylist.append(8)
    mylist.remove(8)
    mylist.insert(6, 10)

    print(mylist, mylist.size(), mylist.search(77), mylist.get_tail())

main()