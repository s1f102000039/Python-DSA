class Queue:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.insert(0,item)

    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)
    
    def __str__(self):
        return str(self.items)

def queue_maker(name_list):
    players = Queue()
    for player in name_list:
        players.enqueue(player)
    return players

def hot_potato(name_list, detonation):
    players = queue_maker(name_list)

    while players.size() > 1:
        
        for i in range(detonation):
            current_player = players.dequeue()
            players.enqueue(current_player)
        
        players.dequeue()

    last_player = players.dequeue()

    return last_player

print(hot_potato(["Bill","David","Susan","Jane","Kent","Brad"],7))

