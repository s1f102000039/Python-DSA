from asyncio import format_helpers


class Deque:
    def __init__(self):
        self.items = []
    
    def isEmpty(self):
        return self.items == []
    
    def addFront(self, item):
        self.items.append(item)
        
    def addRear(self, item):
        self.items.insert(0, item)

    def removeFront(self):
        return self.items.pop()

    def removeRear(self):
        return self.items.pop(0)

    def size(self):
        return len(self.items)

def palindrome_checker(string):
    word = Deque()
    palindrome = True
    sanitzed_string = string.replace(' ', '')
    
    for char in sanitzed_string:
        word.addRear(char)
    
    while word.size() > 1 and palindrome:
        front = word.removeFront()
        rear = word.removeRear()
        if front == rear:
            continue
        palindrome = False
    return palindrome

print(palindrome_checker('I PREFER PI'))
