from asyncio import current_task
import random

class Queue:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def enqueue(self, item):
        self.items.insert(0,item)

    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)
    
    def __str__(self):
        return str(self.items)

class Printer:
    def __init__(self, ppm):
        self.current_task = None
        self.ppm = ppm
        self.wait_time = 0
    
    def print(self):
        if self.current_task != None:
            self.wait_time -= 1
            if self.wait_time == 0:
                self.current_task = None
    
    def get_task(self, task):
        self.current_task = task
        self.wait_time = task.pages*180

    def is_busy(self):
        return self.current_task != None

class Task:
    def __init__(self, time):
        self.pages = random.randrange(1,21)
        self.timestamp = time
    
    def get_stamp(self):
        return self.timestamp

    def get_pages(self):
        return self.pages

    def wait_time(self, currentTime):
        return currentTime - self. timestamp

def print_new_task():
    num = random.randrange(1,181)
    return num == 180

def simulation(ppm, seconds):

    printer = Printer(ppm)
    printQueue = Queue()
    waittime_list = []
    
    for i in range(seconds):
        if print_new_task():
            task = Task(i)
            printQueue.enqueue(task)

        if (not printer.is_busy() and not printQueue.isEmpty()):
            next_task = printQueue.dequeue()
            waittime_list.append(next_task.wait_time(i))
            printer.get_task(next_task)
        
        printer.print()
    
    averageWait=sum(waittime_list)/len(waittime_list)
    print("Average Wait %6.2f secs %3d tasks remaining."%(averageWait,printQueue.size()))


for i in range(10):
    simulation(100,3600)
    

    


