from random import randint

def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_stop =  10**n
    return randint(range_start, range_stop)

print(random_with_N_digits(3))

def toStr(n,base):
    convertString = "0123456789ABCDEF"
    if n < base:
        return convertString[n]
    else:
        return toStr(n//base,base) + convertString[n%base]

print(toStr(12345, 10))