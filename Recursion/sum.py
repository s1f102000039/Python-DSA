def my_sum(lst):
    def sum_iter(n , res):
        if n == len(lst):
            return res
        else: 
            return sum_iter(n + 1, res + lst[n])
    return sum_iter(0, 0)

print(my_sum([1,2,3,4,5]))
    
    