class Stack:
    def __init__(self):
        self.elements = []

    def push(self, item):
        self.elements.append(item)

    def pop(self):
        return self.elements.pop()

    def isEmpty(self): 
        if self.elements:
            return False
        return True

    def peek(self):
        if self.elements:
            return self.elements[-1]
        else:
            return 0

    def size(self):
        return len(self.elements)

    def __str__(self):
        return str(self.elements)

def balance(str):
    s = Stack()
    balanced = True
    paren = [i for i in str if i in ['(',')','{','}','[',']']  ]
    print(paren)
    
    for i in paren:
        if i in ['(','{','[']:
            s.push(i)
        elif i in [')','}',']'] and not s.isEmpty():
            cur_paren = s.pop()
            if cur_paren == '(' and i != ')':
                return False
            if cur_paren == '{' and i != '}':
                return False
            if cur_paren == '[' and i != ']':
                return False
        else:
            balanced = False
    
    if not s.isEmpty():
        balanced = False
    
    return balanced

print(balance("[{(2*3)+ (5*6)+7}"))

