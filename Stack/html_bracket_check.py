import re

class Stack:
    def __init__(self):
        self.elements = []

    def push(self, item):
        self.elements.append(item)

    def pop(self):
        return self.elements.pop()

    def isEmpty(self): 
        if self.elements:
            return False
        return True

    def peek(self):
        if self.elements:
            return self.elements[-1]
        else:
            return 0

    def size(self):
        return len(self.elements)

    def __str__(self):
        return str(self.elements)

def balance(str):
    s = Stack()
    balanced = True
    paren = re.findall(r"<\w+>|<\/\w+>", str)
    
    for i in paren:
        if i[0] == '<' and i[1] != '/':          #If opening bracket then push to stack
            s.push(i)
        elif i[1] == '/' and not s.isEmpty():    #If closing bracket
            cur_paren = s.pop()
            if cur_paren[1:] != i[2:]:           #Check inside content of the bracket
                return False                     #For example, cur_paren = <title> and i = </title>
        else:
            balanced = False
    
    if not s.isEmpty():
        balanced = False
    
    return balanced

print(balance("<html> <head> <title> Example </title> </head> <body> <h1> Hello, world </h1> </body> </html>"))

