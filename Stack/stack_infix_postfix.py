class Stack:
    def __init__(self):
        self.elements = []

    def push(self, item):
        self.elements.append(item)

    def pop(self):
        return self.elements.pop()

    def isEmpty(self): 
        if self.elements:
            return False
        return True

    def peek(self):
        if self.elements:
            return self.elements[-1]
        else:
            return 0

    def size(self):
        return len(self.elements)

    def __str__(self):
        return str(self.elements)

def infixToPostfix(str):
    opStack = Stack()
    result = []
    tokens = str.split(' ')
    precedence = {}
    precedence['+'] = 2
    precedence['-'] = 2
    precedence['*'] = 3
    precedence['/'] = 3
    precedence['('] = 1
    
    
    for token in tokens:
        
        if token.isalnum():
            result.append(token)
        
        elif token == '(':
            opStack.push(token)

        elif token == ')':
            topToken = opStack.pop()
            while topToken != '(':
                result.append(topToken)
                topToken = opStack.pop()
        
        elif token in precedence and token:
            while not opStack.isEmpty() and \
            precedence[opStack.peek()] >= precedence[token]:
                result.append(opStack.pop())
            opStack.push(token)


    while not opStack.isEmpty():
        result += opStack.pop()

    return " ".join(result)


def postFixEval(str):
    tokens = str.split(" ")
    opList = ['+', '-', '*', '/']
    operantStack = Stack()

    for token in tokens:
        if token.isalnum():
            operantStack.push(token)
        elif token in opList and not operantStack.isEmpty():
            first = operantStack.pop()
            second = operantStack.pop()
            operantStack.push(eval(f"{second} {token} {first}"))

    return operantStack.pop()

print(postFixEval("17 10 + 3 * 9 /"))

#A B + C * -> (A + B) * C

def postfixToInfix(str: str) -> str:
    res = ""
    tokens = str.split(" ")
    operandStack = Stack()
    opList = ['+','-','*', '/']


    for token in tokens: 
        if token.isalnum():
            operandStack.push(token)
        
        elif token in opList and not operandStack.isEmpty():
            first = operandStack.pop()
            second = operandStack.pop()
            expression = f"({second} {token} {first})"   
            operandStack.push(expression)

    return operandStack.pop()

print(postfixToInfix("17 10 + 3 * 9 /"))





            
        
    
    


            