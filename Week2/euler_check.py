import networkx as nx

g = nx.read_edgelist('Week2/test.edgelist', nodetype = int)

def euler_check(g):
    if not g.nodes:
        return -1 

    odd_degree = 0
    for degree in g.degree:
        if degree[1]%2 == 1:
            odd_degree += 1
    return odd_degree == 0

print(euler_check(g))

