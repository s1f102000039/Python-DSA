import networkx as nx


G = nx.read_weighted_edgelist('Week3/dij.edgelist', nodetype = int)

D = [float('inf')] * nx.number_of_nodes(G)


def my_extract_min(D,X):
    min_value = float('inf')
    min_index = -1
    for i in range(len(D)):
        if D[i] < min_value:
            if i in X:
                min_index = i
                min_value = D[i]
    return min_index
            

def djikstra(s,G):
    if not G.nodes:
        return -1

    D = [float('inf')] * nx.number_of_nodes(G)  # Initiate array of shortest paths, every elements to infinity
    X = set(G.nodes)                            # Set 's' to 0
    D[s] = 0

    while X:                                    # Until there are no edges left in X, keep iterating
        min_vertex = my_extract_min(D,X)        # Find the smallest path in D
        if min_vertex in X:                     
            X.remove(min_vertex)                # Remove smallest path from X
        for u,v in G.edges(min_vertex):
            if D[v] > D[u] + int(G.edges[u,v]['weight']):
                D[v] = D[u] + int(G.edges[u,v]['weight'])
    return D

result = djikstra(0, G)
for i in range(len(result)):
    print(f"Shortest path to vertex {i}: {result[i]}")
