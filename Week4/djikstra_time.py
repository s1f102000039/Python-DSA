import networkx as nx
import time
import random

def gen_graph(vertices):

    G = nx.fast_gnp_random_graph(vertices, 0.01)

    for (u,v) in G.edges():
        G.edges[u,v]['weight'] = random.randint(1,100)

    nx.write_weighted_edgelist(G, "Week4/weighted_edgelist.edgelist")

def read_graph():                               # Make read graph a seperate function to avoid race condition
    G = nx.read_weighted_edgelist('Week4/weighted_edgelist.edgelist', nodetype = int)
    return G

def my_extract_min(D,X):
    min_value = float('inf')
    min_index = -1
    for i in range(len(D)):
        if D[i] < min_value:
            if i in X:
                min_index = i
                min_value = D[i]
    return min_index
            

def djikstra(s,G):                              # 's' is source vertice
    if not G.nodes:
        return -1

    D = [float('inf')] * nx.number_of_nodes(G)  # Initiate array of shortest paths, every elements to infinity
    X = set(G.nodes)                            # Set 's' to 0
    D[s] = 0

    while X:                                    # Until there are no edges left in X, keep iterating
        min_vertex = my_extract_min(D,X)        # Find the smallest path in D
        if min_vertex in X:                     
            X.remove(min_vertex)                # Remove smallest path from X
        for u,v in G.edges(min_vertex):
            if D[v] > D[u] + int(G.edges[u,v]['weight']):
                D[v] = D[u] + int(G.edges[u,v]['weight'])
    return D


if __name__ ==  '__main__':
    for vertice_count in range(500, 5500, 500):    #Calculating (1000, 11000, 1000) takes too much time)
        gen_graph(vertice_count)
        G = read_graph()
        start = time.time()
        djikstra(0, G)
        end = time.time()
        print(f"Calculating {G} takes {end - start} seconds")



