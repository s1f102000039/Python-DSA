import networkx as nx


def read_graph():
    G = nx.read_weighted_edgelist('Week5/test.edgelist', nodetype=int)

    return G


def my_extract_min(D, X):
    min_value = float('inf')
    min_index = -1
    for i in range(len(D)):
        if D[i] < min_value:
            if i in X:
                min_index = i
                min_value = D[i]
    return min_index


def prim(s, G):
    if not G.nodes:
        return -1

    A = []
    D = [float('inf')] * nx.number_of_nodes(G)
    X = set(G.nodes)
    D[s] = 0
    Pi = [-1] * nx.number_of_nodes(G)

    while X:
        min_vertex = my_extract_min(D, X)
        if min_vertex in X:
            X.remove(min_vertex)
            A.append((Pi[min_vertex], min_vertex))
        for u, v in G.edges(min_vertex):
            if D[v] > int(G.edges[u, v]['weight']):
                D[v] = int(G.edges[u, v]['weight'])
                Pi[v] = u
    return A


print(prim(0, read_graph())[1::])  # The first node is redundant
