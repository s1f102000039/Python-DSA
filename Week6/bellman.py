import networkx as nx

G = nx.read_weighted_edgelist(
    "Week6/bf.edgelist", create_using=nx.DiGraph(), nodetype=int)


def my_Bellman_Ford(G, source):

    n = nx.number_of_nodes(G)
    D = [float('inf')] * n
    D[source] = 0

    for _ in range(1, n):
        D_new = D[:]
        for u, v in G.edges():
            if D_new[u] != float('inf') and D_new[u] + G.edges[u, v]['weight'] < D[v]:
                D_new[v] = D_new[u] + G.edges[u, v]['weight']
        D = D_new
    for u, v in G.edges():
        if D[u] != float('inf') and D[u] + G.edges[u, v]['weight'] < D[v]:
            return(False, D)

    return(True, D)


flag, D = my_Bellman_Ford(G, 0)
print(flag)
print(D)
