import heapq

def lastStoneWeight(stones: list[int]) -> int:
    stones = [-x for x in stones]
    heapq.heapify(stones)
    
    while len(stones) > 1:
        first = heapq.heappop(stones)
        second = heapq.heappop(stones)
        if first < second:
            heapq.heappush(stones, first-second)
    if not stones:
        return 0
    return abs(stones[0])
print(lastStoneWeight([3,7,2]))
        